# config.yml

```
port: 54233
logFile: "log.log"
apps:
  - name: hello
    path: test/hello
    secret: test
```

# hook url:

http://127.0.0.1:54233/hook?secret=test&app=hello&deploy=1&gitpull=1
// if deploy=1
docker-compose pull
docker-compose restart

// if gitpull=1
git pull
