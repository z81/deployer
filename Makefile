VERSION := "0.0.1"

deps:
	go get -u github.com/mitchellh/gox
	go get -u github.com/jinzhu/configor
	go get -u github.com/sirupsen/logrus

build:
	@mkdir -p ./bin
	@gox -output "bin/{{.Dir}}_${VERSION}_{{.OS}}_{{.Arch}}" -os="linux" -os="darwin" -arch="386" -arch="amd64" ./src