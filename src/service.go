package main

import (
	"strconv"
)

func stop() {
	pid := strconv.Itoa(readPid())

	if pid != "0" {
		cmd := execCommand("kill", pid)
		cmd.Start()
	}
}

func start(path string) {
	cmd := execCommand(path, "run_background")
	cmd.Start()

	writePid(cmd.Process.Pid)
}

func restart(path string) {
	stop()
	start(path)
}
