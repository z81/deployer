package main

import (
	"fmt"
	"os"

	"github.com/jinzhu/configor"
	log "github.com/sirupsen/logrus"
)

type App struct {
	Name   string
	Path   string
	Secret string
}

type Config struct {
	Port    uint   `default:"54233"`
	LogFile string `default:"log.log"`
	Apps    []App
}

var tmpPidFile = os.TempDir() + "__deployer__daemon_pid"

var config = Config{}

func initLogger() {
	file, err := os.OpenFile(config.LogFile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0755)
	if err != nil {
		log.Fatal(err)
	}

	log.SetFormatter(&log.TextFormatter{
		DisableColors: true,
	})

	log.SetOutput(file)
}

func main() {
	configor.Load(&config, "config.yml")
	initLogger()

	if len(os.Args) < 2 {
		fmt.Print("Commands:\nstart - run server\nstop - stop server\rrestart - restart server\n")
		return
	}

	cmd := os.Args[1]
	path := os.Args[0]

	switch cmd {
	case "stop":
		stop()
	case "start":
		stop()
		start(path)
	case "restart":
		stop()
		start(path)
	case "run_background":
		runServer(config)
	}
}
