package main

import (
	"bufio"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"

	log "github.com/sirupsen/logrus"
)

func execCommand(name string, args ...string) *exec.Cmd {
	cmd := exec.Command(name, args...)

	go func() {
		stdErrPipe, _ := cmd.StderrPipe()
		scanner := bufio.NewScanner(stdErrPipe)
		for scanner.Scan() {
			log.Warn(scanner.Text())
		}
	}()

	go func() {
		stdOutPipe, _ := cmd.StdoutPipe()
		scanner := bufio.NewScanner(stdOutPipe)
		for scanner.Scan() {
			log.Info(scanner.Text())
		}
	}()

	return cmd
}

func cretePidFileIfNotExists() {
	if _, err := os.Stat(tmpPidFile); os.IsNotExist(err) {
		os.OpenFile(tmpPidFile, os.O_RDONLY|os.O_CREATE, 0644)
	}
}

func writePid(pid int) {
	cretePidFileIfNotExists()

	data := []byte(strconv.Itoa(pid))
	err := ioutil.WriteFile(tmpPidFile, data, 0644)
	if err != nil {
		panic(err)
	}
}

func readPid() int {
	data, err := ioutil.ReadFile(tmpPidFile)
	if err != nil {
		log.Fatal(err)
	}

	if len(data) == 0 {
		return 0
	}

	pid, err := strconv.Atoi(string(data))
	if err != nil {
		log.Fatal(err)
	}

	return pid
}
