package main

import (
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"
)

func getQueryParameter(r *http.Request, key string, defaultValue string) string {
	keys, ok := r.URL.Query()[key]
	if !ok || len(keys[0]) < 1 {
		return defaultValue
	}

	return keys[0]
}

func handlerWithSecret(config Config) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		secret := getQueryParameter(r, "secret", "")
		appName := getQueryParameter(r, "app", "")
		gitPull := getQueryParameter(r, "gitpull", "")
		composeDeploy := getQueryParameter(r, "deploy", "")

		if secret == "" || appName == "" {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		var app App

		for _, confApp := range config.Apps {
			if confApp.Name == appName && confApp.Secret == secret {
				app = confApp
			}
		}

		if app.Name == "" {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		if gitPull != "" {
			cmd := execCommand("git", "pull")
			cmd.Dir = app.Path
			err := cmd.Run()

			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			log.Info("git pull success")
		}

		if composeDeploy != "" {
			cmd := execCommand("docker-compose", "pull")
			cmd.Dir = app.Path
			err := cmd.Run()
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			log.Info("docker-compose pull success")

			cmd = execCommand("docker-compose", "restart")

			cmd.Dir = app.Path
			err = cmd.Run()
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			log.Info("docker-compose restart success")
		}

		w.WriteHeader(http.StatusOK)
	}
}

func runServer(config Config) {
	port := fmt.Sprint(config.Port)

	http.HandleFunc("/hook", handlerWithSecret(config))
	err := http.ListenAndServe(":"+port, nil)

	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	} else {
		fmt.Printf("Listening :" + port)
	}
}
